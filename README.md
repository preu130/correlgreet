# correlgreet

This is a toy R package to greet CorrelAid folks. Toy project for the _Test-driven-development_ workshop at the CorrelAid meetup in Berlin, 30. November 2019.


## Your tasks

1. We want to create a function, that greets our CorrelAid members. Therefore, write a function that returns a greeting in English.
2. As CorrelAid becomes more international, we want to extend the greet function, so we can greet in all languages, where CorrelAid is represented (German, English, Dutch, French). The greet function should therefore take the language as input and return the proper greeting. If no language is specified, greet in English.
3. To personalize the greeting even more, we also want to include the name of the person to be greeted. If no name is specified, greet without a name.
4. Change the greeting according to the daytime. Pretend that you have a function "get_daytime" that returns "morning", "noon", or "evening" depending on the current datetime by mocking the function (yep, that really works!).
5. Use travis (change your remote to a github repo - ask me for help!) or GitLab CI for your project so that your tests run on GitHub / GitLab automatically every time you push.

**Remember**: try to follow the _test-driven-development_ approach - first write the test(s), then the code. :)


# Setup

To work on the package, you have two options:

clone or download the repository

**OR**

create the basic skeleton yourself

## Clone / download repository

If you know how to use git, clone the repository using your git tool of choice. If you later want to work with travis CI, you'll need to change the git remote later. I can help you with that though, no worries! :blush:

If you don't know git yet, you can download the folder as a zip from gitlab. You need to extract the zip on your machine. If you need help with this, please ask me. :blush:

![](img/download_button.png)

## Create yourself

1. install the `usethis` package
2. create a package yourself with `usethis::create_package()` from the R console in RStudio
3. add `tests` folder and the necessary files with `usethis::use_testthat()`

```
usethis::create_package("path/to/correlgreet")
```

## Install devtools

Install the `devtools` package. This is a handy package that helps with R package development.

Useful functions:

- `devtools::load_all()`: "load_all loads a package. It roughly simulates what happens when a package is installed and loaded with library()." (RStudio shortcut: CTRL+SHIFT+L)
- `devtools::test()`: run all unit tests in `tests` folder. See `?devtools::test()` for other similar commands (RStudio shortcut: ALT+SHIFT+T / CMD+SHIFT+T)
- `devtools::document()`: (re-)create documentation in `man` folder by running `roxygenize`. (RStudio shortcut: CTRL+SHIFT+D -> if this does not work, check that you have [roxgen configured correctly](https://community.rstudio.com/t/build-package-documentation-shortcut-not-working/36201))
- `devtools::build()`: build the package into a single bundled file (RStudio shortcut: CTRL+SHIFT+B)

## Open R Packages book
More info on building packages: [http://r-pkgs.had.co.nz/](http://r-pkgs.had.co.nz/)

# Writing tests

## Process

- generate a file in the `tests/testthat` folder. Call it `functionname_test.R` where `functionname` is the name of your function.
- write a test that fails (bc no code has been written yet)
- watch it fail!
- implement enough code in the `R` folder to make it pass
- run `devtools::load_all()` and then the test and watch it pass
- repeat

## testthat package

general structure of a test:

```
test_that("function a has behaviour x", {
    expect_*(function_call(), expected_value)
})

```

the testthat package provides several `expect_*` functions that make it easy to test behaviour. For example:

- `expect_equal(x, y)`: tests that x and y are equal
- `expect_lt(x, y)`: tests that x is less than y
- `expect_true(x)`: tests that x is true
- `expect_error(x())`: tests that x returns an error

See the [package reference](https://testthat.r-lib.org/reference/index.html) for all `expect` methods.

# Mocking 

What is mocking? 
-> a good answer can be found in this [Stackoverflow answer](https://stackoverflow.com/a/2666006/6276235).

In R, we can *mock* using the [mockery](https://github.com/r-lib/mockery) package. The functionality is not as good as in python but for some use cases, it is sufficient. 

Examples where this might be useful:

- you make a call to the API. Instead of *really* making the call, you mock the return value of the call.
- your functionality depends on things like the current time (see task 4)
- ...

# CI/CD
